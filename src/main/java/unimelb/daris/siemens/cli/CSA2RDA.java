package unimelb.daris.siemens.cli;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import unimelb.daris.siemens.csa.CSADicomFile;

public class CSA2RDA {

	public static void main(String[] args) {
		try {
			/*
			 * validate arguments
			 */
			if (args.length < 1 || args.length > 2) {
				throw new IllegalArgumentException("Missing or invalid arguments.");
			}
			Path inputCSAFile = Paths.get(args[0]);
			if (!Files.isRegularFile(inputCSAFile)) {
				throw new IllegalArgumentException(
						"Input CSA DICOM file: '" + inputCSAFile + "' does not exist or is not a regular file.");
			}
			Path outputRDAFile = null;
			if (args.length == 2) {
				outputRDAFile = Paths.get(args[1]);
			} else {
				String inputCSAFileName = inputCSAFile.getFileName().toString();
				String outputRDAFileName = (inputCSAFileName.toLowerCase().endsWith(".dcm")
						? inputCSAFileName.substring(0, inputCSAFileName.length() - 4)
						: inputCSAFileName) + ".rda";
				outputRDAFile = Paths.get(inputCSAFile.getParent().toString(), outputRDAFileName);
			}
			if (Files.exists(outputRDAFile)) {
				throw new IllegalArgumentException("Output RDA file: '" + outputRDAFile + "' already exists.");
			}

			/*
			 * convert CSA to RDA
			 */
			System.out.printf("Converting '%s' to '%s' ... ", inputCSAFile, outputRDAFile);
			CSADicomFile.toRDA(inputCSAFile, outputRDAFile);
			System.out.println("done.");
		} catch (Throwable t) {
			if (t instanceof IllegalArgumentException) {
				System.err.println("Error: " + t.getMessage());
				printUsage();
			} else {
				t.printStackTrace();
			}
			System.exit(1);
		}
	}

	private static void printUsage() {
		System.out.println();
		System.out.println("NAME");
		System.out.println("    csa2rda -- convert Siemens CSA DICOM file to Siemens RDA file");
		System.out.println();
		System.out.println("SYNOPSIS");
		System.out.println("    csa2rda <input_csa_dcm_file> [output_rda_file]");
		System.out.println();
	}

}
