package unimelb.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CountedInputStream extends FilterInputStream {

    private long _bytesCount = 0;
    private long _mark = -1;

    public CountedInputStream(InputStream in) {
        super(in);
    }

    /**
     * Number of bytes have been read.
     * 
     * @return
     */
    public synchronized long bytesCount() {
        return _bytesCount;
    }

    @Override
    public int read() throws IOException {
        int result = in.read();
        if (result != -1) {
            incBytesCount(1);
        }
        return result;
    }

    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        int result = in.read(b, off, len);
        if (result != -1) {
            incBytesCount(result);
        }
        return result;
    }

    @Override
    public long skip(final long n) throws IOException {
        long result = in.skip(n);
        incBytesCount(result);
        return result;
    }

    @Override
    public void mark(final int readlimit) {
        in.mark(readlimit);
        _mark = bytesCount();
        // it's okay to mark even if mark isn't supported, as reset won't work
    }

    @Override
    public void reset() throws IOException {
        if (!in.markSupported()) {
            throw new IOException("Mark not supported");
        }
        if (_mark == -1) {
            throw new IOException("Mark not set");
        }
        in.reset();
        setBytesCount(_mark);
    }

    protected synchronized void incBytesCount(final long increment) {
        _bytesCount += increment;
    }

    protected synchronized void setBytesCount(final long bytesCount) {
        _bytesCount = bytesCount;
    }
}