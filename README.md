# csa2rda

A command line utility to convert Siemens CSA DICOM file to Siemens RDA file.

## Build

- Java SDK 8+ and Maven are required to build the tool from the source code.

```
git clone https://gitlab.unimelb.edu.au/wliu5/csa2rda.git
cd csa2rda
mvn clean package
```

The result package will be `target/csa2rda-0.0.1.zip`


### Usage

1. Install the package:
```
unzip csa2rda-0.0.1.zip
```
2. The executable script `csa2rda` (or `csa2rda.cmd` on Windows) is inside the directory:
```
cd csa2rda-0.0.1
```
3. Convert a Siemens CSA DICOM file to Siemens RDA file:
```
./csa2rda /path/to/test-csa.dcm ./test-csa.RDA
```


